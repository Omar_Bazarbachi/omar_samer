/**
 * \file askingquestion.h
 * \brief The AskingQuestion class acts as the widget that shows the user the question and records his anser in game 1
 */
#ifndef ASKINGQUESTION_H
#define ASKINGQUESTION_H

#include <QWidget>
#include <QDialog>
#include <QtWidgets>
#include <QKeyEvent>

class AskingQuestion : public QDialog
{
    Q_OBJECT
public:
    explicit AskingQuestion(QWidget *parent = nullptr, QString str="");
    void keyPressEvent(QKeyEvent* event);
    int isCorrect=-1;
    QString line;
    QString correctanswer;

    QVector<int> findLocations(QString str){
        QVector<int> locations;
        for(int i =0;i < str.size();i++){
            if(str[i]==':'){
                locations.push_back(i);
            }
        }
        return locations;
    }

private:
    QRadioButton *a;
    QRadioButton *b;
    QRadioButton *c;
    QRadioButton *d;

signals:

public slots:
    void submit();

};

#endif // ASKINGQUESTION_H
