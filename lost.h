/**
 * \file lost.h
 * \brief The Lost class is a widget that tells the user they have lost
 */
#ifndef LOST_H
#define LOST_H

#include <QWidget>
#include <QtWidgets>
#include "Games.h"

class Lost : public QWidget
{
    Q_OBJECT
public:
    explicit Lost(QWidget *widget=nullptr, QString name="", int score=0, int game=0);

private:
    QWidget *wid;
    QString uname;
    int score;
    int game;
    QLabel *message;
    QPushButton *leave;
    QGridLayout *grid;
    void saveScore();

public slots:
    void leavef();

};

#endif // LOST_H
