#include "game2scene.h"
/**
 * \file game2scene.cpp
 * \brief game2scene.cpp Sets the scene for Game2
 * \param parent a null pointer given to the QGraphicsScene constructer, widget a widget that is the parent of this scene, uname a QString that is the user name of the current user
 */
Game2Scene::Game2Scene(QGraphicsScene *parent, QWidget *widget, QString name) : QGraphicsScene(parent), widget(widget), uname(name) {

    scoretoshow = new QLabel("Score :0");
    Game2 *parentwid = (Game2 *) widget;
    parentwid->grid->addWidget(scoretoshow);
    srand(time(0));
    setBackgroundBrush(QBrush(QImage(":/images/background2.png").scaledToHeight(1000).scaledToWidth(400)));
    setSceneRect(0,0,400,1000);
    thequeue = QQueue<QObject*>();
    score = 0;
    missed = 0;
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(2000);
    timer2 = new QTimer(this);
    connect(timer2, SIGNAL(timeout()), this, SLOT(checklost()));
    timer2->start(100);
    theme.setMedia(QUrl("qrc:/sounds/02. Game Theme.mp3"));
    theme.setVolume(50);
    theme.play();
}

void Game2Scene::keyPressEvent(QKeyEvent *event){
    if(thequeue.isEmpty()){
        updatemissed();
    }else{
        Disk *head = (Disk *) thequeue.head();
        if (event->key() == Qt::Key_Right) {
            if (head->y() >= 620 && head->y()<780 && head->value==2) {
                updatescore(7);
                Disk *todelete = (Disk *) thequeue.dequeue();
                delete todelete;
                QMediaPlayer sound;
                sound.setMedia(QUrl("qrc:/sounds/se_game_triple.wav"));
                sound.setVolume(50);
                sound.play();
            } else {
                updatemissed();
            }
        }
        if (event->key() == Qt::Key_Left) {
            if (head->y() >= 620 && head->y()<780 && head->value==0) {
                updatescore(3);
                Disk *todelete = (Disk *) thequeue.dequeue();
                delete todelete;
                QMediaPlayer sound;
                sound.setMedia(QUrl("qrc:/sounds/se_game_single.wav"));
                sound.setVolume(50);
                sound.play();
            } else {
                updatemissed();
            }
        }
        if (event->key() == Qt::Key_Down) {
            if (head->y() >= 620 && head->y()<780 && head->value==1) {
                updatescore(5);
                Disk *todelete = (Disk *) thequeue.dequeue();
                delete todelete;
                QMediaPlayer sound;
                sound.setMedia(QUrl("qrc:/sounds/se_game_double.wav"));
                sound.setVolume(50);
                sound.play();
            } else {
                updatemissed();
            }
        }
    }
}
void Game2Scene::update() {
    int position = rand()%3;
    if(position==0){
        Disk *theDisk = new Disk(nullptr,this->widget,this,uname,0);
        addItem(theDisk);
        thequeue.enqueue(theDisk);
        theDisk->setPos(50, 0);
    } else if(position==1){
        Disk *theDisk = new Disk(nullptr,this->widget,this,uname,1);
        addItem(theDisk);
        thequeue.enqueue(theDisk);
        theDisk->setPos(156, 0);
    } else if(position==2){
        Disk *theDisk = new Disk(nullptr,this->widget,this,uname,2);
        addItem(theDisk);
        thequeue.enqueue(theDisk);
        theDisk->setPos(266, 0);
    }
}

void Game2Scene::updatescore(int i){
    score+=i;
    scoretoshow->setText("Score :" + QString::number(score));
}

void Game2Scene::updatemissed(){
    missed+=1;
}

void Game2Scene::checklost(){
    if(missed >= 3){
        if(score< 150){
            Lost *widget = new Lost(this->widget,uname,score,2);
            QDesktopWidget w1;
            QRect screenGeometry = w1.screenGeometry();
            int w = screenGeometry.width();
            int h = screenGeometry.height();
            widget->setGeometry((w-widget->width())/2,(h-widget->height())/2,widget->width(),widget->height());
            widget->show();
        }
        else{
            Won *widget = new Won(this->widget,uname,score,2);
            QDesktopWidget w1;
            QRect screenGeometry = w1.screenGeometry();
            int w = screenGeometry.width();
            int h = screenGeometry.height();
            widget->setGeometry((w-widget->width())/2,(h-widget->height())/2,widget->width(),widget->height());
            widget->show();
        }
        timer->stop();
        timer2->stop();
    }
}
