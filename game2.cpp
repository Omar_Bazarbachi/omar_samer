#include "game2.h"
/**
 * \file game2.cpp
 * \brief game2.cpp Initializes Game 2 GUI
 * \param parent a null pointer given to the QWidget constructer, uname a QString that is the username of the current user
 */
Game2::Game2(QWidget *parent, QString name) : QWidget(parent), uname(name) {

    grid = new QGridLayout;
    leave = new QPushButton("leave game");
    scene = new Game2Scene(nullptr,this,uname);
    QGraphicsView *view = new QGraphicsView();
    view->setScene(scene);
    view->setHorizontalScrollBarPolicy((Qt::ScrollBarAlwaysOff));
    view->setVerticalScrollBarPolicy((Qt::ScrollBarAlwaysOff));
    grid->addWidget(leave);
    grid->addWidget(view);
    setLayout(grid);
    setFixedSize(400,1000);
    QObject::connect(leave, SIGNAL(clicked()), this, SLOT(leavef()));
    this->setAttribute(Qt::WA_DeleteOnClose);
}

/**
 * \brief    Leaves Game 2
*/
void Game2::leavef() {
    Games *widget;
    if(uname==""){
        widget = new Games();
    }else{
    QFile file("/home/eece435l/omar_samer/databases/creds_db.txt");
    QString line;
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream in(&file);
    while(!in.atEnd()) {
        line = in.readLine();
        if (line.mid(0,uname.size())==uname){
            widget = new Games(line.toStdString());
        }
    }
    }
    QDesktopWidget w1;
    QRect screenGeometry = w1.screenGeometry();
    int w = screenGeometry.width();
    int h = screenGeometry.height();
    widget->setGeometry((w-widget->width())/2,(h-widget->height())/2,widget->width(),widget->height());
    widget->show();
    delete scene;
    this->close();
}

