/**
 * \file Games.h
 * \brief The Games class is the widget where the user can choose what game to play or to open his/her history
 */
#ifndef GAMES_H
#define GAMES_H

#include <QWidget>
#include <QtWidgets>
#include "InitialWidget.h"
#include "game1.h"
#include "game2.h"
#include "gamehistory.h"
#include "questiondialogue.h"
#include <vector>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QKeyEvent>

using namespace std;

class Games : public QWidget
{
    Q_OBJECT
public:
    explicit Games(QWidget *parent = nullptr);
    Games(string str);
    void keyPressEvent(QKeyEvent* event);
private:
    QPushButton *logoutbutton;
    QPushButton *playgame1;
    QPushButton *playgame2;
    QPushButton *historygame1;
    QPushButton *historygame2;
    QVBoxLayout *Vertical;
    QGridLayout *grid;
    QHBoxLayout *country;
    QHBoxLayout *buttons;
    string username="";
    string firstnamestr;
    string lastnamestr;
    string countrynumber;
    string picpath;
    QLabel *firstname;
    QLabel *lastname;
    QLabel *currentdate;
    QLabel *countryname;
    QLabel *test;
    QLabel *test2;

    QVector<int> findLocations(string str){
        QVector<int> locations;
        for(int i =0;i < str.size();i++){
            if(str[i]==':'){
                locations.push_back(i);
            }
        }
        return locations;
    }
public slots:
    void logout();
    void opengame1();
    void opengame2();
    void openhistorygame1();
    void openhistorygame2();


signals:

};

#endif // GAMES_H
