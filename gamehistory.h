/**
 * \file gamehistory.h
 * \brief The GameHistory class is the widget that shows the user their history
 */
#ifndef GAMEHISTORY_H
#define GAMEHISTORY_H

#include <QWidget>
#include <QtWidgets>
#include <QFile>
#include <QTextStream>
#include <QString>
#include "Games.h"
#include <vector>

using namespace std;

class GameHistory : public QWidget
{
    Q_OBJECT
public:
    explicit GameHistory(string username="",int gamenum=0);

private:
    QTableWidget *table;
    QGridLayout *grid;
    QLabel *name;
    QStringList colum_names;
    QPushButton *leave;
    QString uname;
    int gamenumber;
    int highscore;
    QVector<int> findLocations(QString str){
        QVector<int> locations;
        for(int i =0;i < str.size();i++){
            if(str[i]==':'){
                locations.push_back(i);
            }
        }
        return locations;
    }

signals:

public slots:
    void leavef();

};

#endif // GAMEHISTORY_H
