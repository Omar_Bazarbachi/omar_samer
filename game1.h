/**
 * \file game1.h
 * \brief The Game1 class acts as the base of Game 1
 */
#ifndef GAME1_H
#define GAME1_H

#include <QWidget>
#include <QtWidgets>
#include <QGraphicsView>
#include "game1scene.h"
#include "questiondialogue.h"
#include "QDesktopWidget"

class Game1 : public QWidget
{
    Q_OBJECT
public:
    explicit Game1(QWidget *parent = nullptr, QString name="");
    QGraphicsScene *scene;
private:
    QString uname;
    QPushButton *questions;
    QPushButton *leave;
    QGridLayout *grid;
    QGraphicsView *view;
signals:

public slots:
    void openquestions();
    void leavef();

};

#endif // GAME1_H
