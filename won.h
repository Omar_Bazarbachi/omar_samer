/**
 * \file won.h
 * \brief The Won class is a widget that tells the user they have won
 */
#ifndef WON_H
#define WON_H

#include <QWidget>
#include <QtWidgets>
#include "Games.h"

class Won : public QWidget
{
    Q_OBJECT
public:
    explicit Won(QWidget *widget=nullptr, QString name="", int score=0, int game=0);
private:
    QWidget *wid;
    QString uname;
    int score;
    int game;
    QLabel *message;
    QPushButton *leave;
    QGridLayout *grid;
    void saveScore();

public slots:
    void leavef();

};

#endif // WON_H
