#include "askingquestion.h"
/**
 * \file askingquestion.cpp
 * \brief askingquestion.cpp creates the AskingQuestion class to be used
 * \param parent a null pointer given to the QDialog constructer, line a QString that holds a question
 */
AskingQuestion::AskingQuestion(QWidget *parent, QString str) : QDialog(parent), line(str) {
    QVector<int> locations= findLocations(line);
    correctanswer = line.mid(locations.at(5)+1,line.length()-locations.at(5)-1);
    QGridLayout *grid = new QGridLayout;
    QString questionstr = line.mid(locations.at(0)+1,locations.at(1)-locations.at(0)-1);
    QLabel *question = new QLabel(questionstr);
    question->setWordWrap(true);
    QString answerastr=line.mid(locations.at(1)+1,locations.at(2)-locations.at(1)-1);
    QLabel *answera = new QLabel(answerastr);
    QString answerbstr=line.mid(locations.at(2)+1,locations.at(3)-locations.at(2)-1);
    QLabel *answerb = new QLabel(answerbstr);
    QString answercstr=line.mid(locations.at(3)+1,locations.at(4)-locations.at(3)-1);
    QLabel *answerc = new QLabel(answercstr);
    QString answerdstr=line.mid(locations.at(4)+1,locations.at(5)-locations.at(4)-1);
    QLabel *answerd = new QLabel(answerdstr);
    a = new QRadioButton();
    b = new QRadioButton();
    c = new QRadioButton();
    d = new QRadioButton();
    QPushButton *submit = new QPushButton("submit");
    grid->addWidget(question,0,0,2,5);
    grid->addWidget(a,2,0);
    grid->addWidget(answera,2,1,1,3);
    grid->addWidget(b,3,0);
    grid->addWidget(answerb,3,1,1,3);
    grid->addWidget(c,4,0);
    grid->addWidget(answerc,4,1,1,3);
    grid->addWidget(d,5,0);
    grid->addWidget(answerd,5,1,1,3);
    grid->addWidget(submit,6,2);
    setLayout(grid);
    setFixedSize(500,200);
    QObject::connect(submit, SIGNAL(clicked()), this, SLOT(submit()));
    this->setAttribute(Qt::WA_DeleteOnClose);

}
/**
 * \brief this function submits the answer
 *
 * when the button submit is pushed the answer picked by the
 * user is compared to the one in the database and sets
 * isCorrect to 1 if they match or to -1 otherwise.
 */
void AskingQuestion::submit(){
    if(correctanswer=="a" && a->isChecked()){
        isCorrect=1;
    }
    else if(correctanswer=="b" && b->isChecked()){
        isCorrect=1;
    }
    else if(correctanswer=="c" && c->isChecked()){
        isCorrect=1;
    }
    else if(correctanswer=="d" && d->isChecked()){
        isCorrect=1;
    }
    else {
        isCorrect=-1;
    }
    this->close();
}
/**
 * \brief checks if key press is 1 or 2 or 3 or 4
 * to know what answer to pick
 * \param event is the key press that we look at
 *
 * everytime a key is pressed this function will check it
 * if it is the 1 key it will select answer a
 * if it is the 2 key it will select answer b
 * if it is the 3 key it will select answer c
 * if it is the 4 key it will select answer d
 */
void AskingQuestion::keyPressEvent(QKeyEvent *event){
    if (event->key() == Qt::Key_1) {
        a->click();
    }

    if (event->key() == Qt::Key_2) {
        b->click();
    }

    if (event->key() == Qt::Key_3) {
        c->click();
    }

    if (event->key() == Qt::Key_4) {
        d->click();
    }
    if (event->key() == Qt::Key_Enter || event->key() == Qt::Key_Return) {
        submit();
    }
}

