/**
 * \file game2.h
 * \brief The Game2 class acts as the base of Game 2
 */
#ifndef GAME2_H
#define GAME2_H

#include <QWidget>
#include <QtWidgets>
#include <QGraphicsView>
#include "QDesktopWidget"
#include "game2scene.h"

class Game2 : public QWidget
{
    Q_OBJECT
public:
    explicit Game2(QWidget *parent = nullptr, QString uname="");
    QString uname;
    QPushButton *leave;
    QGridLayout *grid;
    QGraphicsScene *scene;
signals:

public slots:
    void leavef();

};

#endif // GAME2_H
