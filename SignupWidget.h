/**
 * \file SignupWidget.h
 * \brief The SignupWidget class is the page where the user can create an account
 */
#ifndef SIGNUP_H
#define SIGNUP_H

#include <QWidget>
#include <QtWidgets>
#include "QLineEdit"
#include <iostream>
#include <fstream>
#include <QFile>
#include <QString>
#include <QTextStream>
#include <QKeyEvent>
#include "Games.h"
#include <regex>
#include <algorithm>

class SignupWidget: public QWidget
{
    Q_OBJECT
public:
    explicit SignupWidget(QWidget *parent = nullptr);
    void keyPressEvent(QKeyEvent* event);
private:
    QLabel *first;
    QLabel *last;
    QLabel *username;
    QLabel *password;
    QLabel *password2;
    QLabel *dateofbirth;
    QLabel *phone;
    QLabel *profilepic;
    QLineEdit *firstl;
    QLineEdit *lastl;
    QLineEdit *usernamel;
    QLineEdit *passwordl;
    QLineEdit *password2l;
    QLineEdit *countrycode;
    QLineEdit *phonenumber;
    QDateEdit *date;
    QLabel *gender;
    QRadioButton *male;
    QRadioButton *female;
    QPushButton *submit;
    QPushButton *uploadpic;
    QGridLayout *grid;
    QGroupBox *radio;
    QVBoxLayout *radiover;
    QHBoxLayout *number;
    QLabel *empty;
    std::string path;
public slots:
    void signup();
    void uploadpicture();
    std::string add2DB();
};

#endif // SIGNUP_H
