QT+=widgets \
    widgets \
    multimedia

SOURCES += \
    Games.cpp \
    InitialWidget.cpp \
    SignupWidget.cpp \
    askingquestion.cpp \
    disk.cpp \
    game1.cpp \
    game1scene.cpp \
    game2.cpp \
    game2scene.cpp \
    gamehistory.cpp \
    lost.cpp \
    main.cpp \
    questiondialogue.cpp \
    won.cpp

HEADERS += \
    Games.h \
    InitialWidget.h \
    SignupWidget.h \
    askingquestion.h \
    disk.h \
    game1.h \
    game1scene.h \
    game2.h \
    game2scene.h \
    gamehistory.h \
    lost.h \
    questiondialogue.h \
    won.h

DISTFILES += \
    Flag-Russia.jpg \
    France.png \
    NorthKorea.png \
    china.png \
    creds_db.txt \
    databases/France.png \
    databases/creds_db.txt \
    databases/flag-400.png \
    databases/historygame1.txt \
    databases/historygame2.txt \
    databases/questions.txt \
    flag-400.png \
    historygame1.txt \
    historygame2.txt \
    index.png

RESOURCES += \
    images.qrc \
    sounds.qrc
