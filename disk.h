/**
 * \file disk.h
 * \brief The disk class is the disk that will fall in Game 2
 */
#ifndef DISK_H
#define DISK_H

#include <QObject>
#include <QGraphicsPixmapItem>
#include <QKeyEvent>
#include <QGraphicsScene>
#include <QTimer>
#include <QKeyEvent>
#include "game2scene.h"

class Disk : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    explicit Disk(QObject *parent = 0, QWidget *widget=nullptr, QGraphicsScene *scene=nullptr, QString name="", int val=0);
    QWidget *widget;
    QGraphicsScene *parentscene;
    QString uname;
    int value;
private:
    QTimer* timer;
    QTimer* timer2;
    int reached30;
    int reached60;
    int reached90;
    int reached120;
    int changeamount;

public slots:
    void update();
    void changeSpeed();

};


#endif // DISK_H
