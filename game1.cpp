#include "game1.h"
/**
 * \file game1.cpp
 * \brief game1.cpp Initializes Game 1 GUI
 * \param parent a null pointer given to the QWidget constructer,uname the user name of the current user
 */

Game1::Game1(QWidget *parent, QString name) : QWidget(parent), uname(name) {

    grid = new QGridLayout;
    questions = new QPushButton("Change Questions");
    leave = new QPushButton("leave game");
    scene = new Game1Scene(nullptr,this,uname);
    view = new QGraphicsView();
    view->setScene(scene);
    view->setHorizontalScrollBarPolicy((Qt::ScrollBarAlwaysOff));
    view->setVerticalScrollBarPolicy((Qt::ScrollBarAlwaysOff));
    grid->addWidget(questions,0,0);
    grid->addWidget(leave,0,1);
    grid->addWidget(view,1,0,1,2);
    setLayout(grid);
    setFixedSize(1000,500);
    QObject::connect(questions, SIGNAL(clicked()), this, SLOT(openquestions()));
    QObject::connect(leave, SIGNAL(clicked()), this, SLOT(leavef()));
    this->setAttribute(Qt::WA_DeleteOnClose);
}
/**
 * \brief Opens the change questions page
 *
 * everytime the questions button is clicked we will open a widget
 * where the user can input new questions and answers
 */
void Game1::openquestions() {
    QuestionDialogue *widget = new QuestionDialogue();
    QDesktopWidget w1;
    QRect screenGeometry = w1.screenGeometry();
    int w = screenGeometry.width();
    int h = screenGeometry.height();
    widget->setGeometry((w-widget->width())/2,(h-widget->height())/2,widget->width(),widget->height());
    widget->show();
    this->close();
}
/**
 * \brief Leaves the game
 *
 * everytime the leave button is clicked we will
 * exit the game and open a new Games widget
 */
void Game1::leavef() {
    if(uname != ""){
        Games *widget;
        QFile file("/home/eece435l/omar_samer/databases/creds_db.txt");
        QString line;
        file.open(QIODevice::ReadOnly | QIODevice::Text);
        QTextStream in(&file);
        while(!in.atEnd()) {
            line = in.readLine();
            if (line.mid(0,uname.size())==uname){
                widget = new Games(line.toStdString());
            }
        }
        QDesktopWidget w1;
        QRect screenGeometry = w1.screenGeometry();
        int w = screenGeometry.width();
        int h = screenGeometry.height();
        widget->setGeometry((w-widget->width())/2,(h-widget->height())/2,widget->width(),widget->height());
        widget->show();
        delete scene;
        this->close();
    } else{
        Games *widget = new Games();
        QDesktopWidget w1;
        QRect screenGeometry = w1.screenGeometry();
        int w = screenGeometry.width();
        int h = screenGeometry.height();
        widget->setGeometry((w-widget->width())/2,(h-widget->height())/2,widget->width(),widget->height());
        widget->show();
        delete scene;
        this->close();
    }
}

