/**
 * \file questiondialogue.h
 * \brief The QuestionDialogue class is the widget where the user can input new questions
 */
#ifndef QUESTIONDIALOGUE_H
#define QUESTIONDIALOGUE_H

#include <QWidget>
#include <QtWidgets>
#include <string>
#include "game1.h"

class QuestionDialogue : public QWidget
{
    Q_OBJECT
public:
    explicit QuestionDialogue(QWidget *parent = nullptr);
private:
    QLabel *prompt;

    QLabel *a[10];
    QLabel *b[10];
    QLabel *c[10];
    QLabel *d[10];

    QRadioButton *abuttons[10];
    QRadioButton *bbuttons[10];
    QRadioButton *cbuttons[10];
    QRadioButton *dbuttons[10];

    QButtonGroup *buttongroup[10];

    QHBoxLayout *Ha[10];
    QHBoxLayout *Hb[10];
    QHBoxLayout *Hc[10];
    QHBoxLayout *Hd[10];

    QLabel *questionlabel[10];
    QLineEdit *question[10];
    QLineEdit *answera[10];
    QLineEdit *answerb[10];
    QLineEdit *answerc[10];
    QLineEdit *answerd[10];

    QPushButton *submit;

    QGridLayout *layout;


public slots:
    void opengame1();
    void SaveQuestions();

signals:

};

#endif // QUESTIONDIALOGUE_H
