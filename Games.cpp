#include "Games.h"
/**
 * \file Games.cpp
 * \brief Games.cpp Initializes the lobby GUI for guests
 * \param parent a null pointer given to the QWidget constructer
 */
Games::Games(QWidget *parent) : QWidget(parent) {

    logoutbutton = new QPushButton("Leave");
    playgame1 = new QPushButton("play Game 1");
    playgame2 = new QPushButton("play Game 2");
    historygame1 = new QPushButton("History Game 1");
    historygame2 = new QPushButton("History Game 2");
    buttons = new QHBoxLayout;
    buttons->addWidget(logoutbutton);
    buttons->addWidget(playgame1);
    buttons->addWidget(playgame2);
    grid = new QGridLayout;
    grid->addLayout(buttons,3,0,1,2);
    grid->addWidget(historygame1,4,0);
    grid->addWidget(historygame2,4,1);
    setLayout(grid);
    setFixedSize(350,100);
    QObject::connect(logoutbutton, SIGNAL(clicked()), this, SLOT(logout()));
    QObject::connect(playgame1, SIGNAL(clicked()), this, SLOT(opengame1()));
    QObject::connect(playgame2, SIGNAL(clicked()), this, SLOT(opengame2()));
    this->setAttribute(Qt::WA_DeleteOnClose);

}

/**
 * \brief Games.cpp Initializes the lobby GUI for users
 * \param str a string that contains the user's information
 */
Games::Games(string str) : username(str.substr(0,findLocations(str).at(0))),
                           firstnamestr(str.substr(findLocations(str).at(1)+1,findLocations(str).at(2)-findLocations(str).at(1)-1)),
                           lastnamestr(str.substr(findLocations(str).at(2)+1,findLocations(str).at(3)-findLocations(str).at(2)-1)),
                           countrynumber(str.substr(findLocations(str).at(3)+1,findLocations(str).at(4)-findLocations(str).at(3)-1)),
                           picpath(str.substr(findLocations(str).at(6)+1,findLocations(str).at(7)-findLocations(str).at(6)-1)) {
    logoutbutton = new QPushButton("Log Out");
    playgame1 = new QPushButton("play Game 1");
    playgame2 = new QPushButton("play Game 2");
    historygame1 = new QPushButton("History Game 1");
    historygame2 = new QPushButton("History Game 2");
    string s1 = "First Name: "+firstnamestr;
    firstname = new QLabel(QString::fromStdString(s1));
    string s2 = "Last Name: "+lastnamestr;
    lastname = new QLabel(QString::fromStdString(s2));
    currentdate = new QLabel(QDate::currentDate().toString());
    if(countrynumber=="+961"){
        countryname = new QLabel("Lebanon");
        QPixmap flag(":/images/index.png");
        test = new QLabel();
        test->setPixmap(flag.scaled(30,20));
    }
    else if(countrynumber=="+33"){
        countryname = new QLabel("France");
        QPixmap flag(":/images/France.png");
        test = new QLabel();
        test->setPixmap(flag.scaled(30,20));
    }
    else if(countrynumber=="+1"){
        countryname = new QLabel("Russia");
        QPixmap flag(":/images/flag-400.png");
        test = new QLabel();
        test->setPixmap(flag.scaled(30,20));
    }
    else if(countrynumber=="+86"){
        countryname = new QLabel("China");
        QPixmap flag(":/images/china.png");
        test = new QLabel();
        test->setPixmap(flag.scaled(30,20));
    }
    else if(countrynumber=="+850"){
        countryname = new QLabel("North Korea");
        QPixmap flag(":/images/NorthKorea.png");
        test = new QLabel();
        test->setPixmap(flag.scaled(30,20));
    }
    else{
        countryname = new QLabel("No country");
        QPixmap flag(":/images/NorthKorea.png");
        test = new QLabel();
        test->setPixmap(flag.scaled(30,20));
    }
    QPixmap pic(QString::fromStdString(picpath));
    test2 = new QLabel();
    test2->setPixmap(pic.scaled(60,60));
    country = new QHBoxLayout;
    country->addWidget(countryname);
    country->addWidget(test);
    buttons = new QHBoxLayout;
    buttons->addWidget(logoutbutton);
    buttons->addWidget(playgame1);
    buttons->addWidget(playgame2);
    grid = new QGridLayout;
    grid->addWidget(test2,0,0,2,1);
    grid->addWidget(firstname,0,1);
    grid->addWidget(lastname,1,1);
    grid->addWidget(currentdate,2,0);
    grid->addLayout(country,2,1);
    grid->addLayout(buttons,3,0,1,2);
    grid->addWidget(historygame1,4,0);
    grid->addWidget(historygame2,4,1);
    setLayout(grid);
    QObject::connect(logoutbutton, SIGNAL(clicked()), this, SLOT(logout()));
    QObject::connect(playgame1, SIGNAL(clicked()), this, SLOT(opengame1()));
    QObject::connect(playgame2, SIGNAL(clicked()), this, SLOT(opengame2()));
    QObject::connect(historygame1, SIGNAL(clicked()), this, SLOT(openhistorygame1()));
    QObject::connect(historygame2, SIGNAL(clicked()), this, SLOT(openhistorygame2()));
    setFixedSize(350,180);
    this->setAttribute(Qt::WA_DeleteOnClose);
}

/**
 * \brief    Handles logout
*/
void Games::logout() {
    InitialWidget *widget = new InitialWidget();
    QDesktopWidget w1;
    QRect screenGeometry = w1.screenGeometry();
    int w = screenGeometry.width();
    int h = screenGeometry.height();
    widget->setGeometry((w-widget->width())/2,(h-widget->height())/2,widget->width(),widget->height());
    widget->show();
    this->close();
}

/**
 * \brief    Opens game 1
*/
void Games::opengame1() {
    Game1 *widget = new Game1(nullptr, QString::fromStdString(username));
    QDesktopWidget w1;
    QRect screenGeometry = w1.screenGeometry();
    int w = screenGeometry.width();
    int h = screenGeometry.height();
    widget->setGeometry((w-widget->width())/2,(h-widget->height())/2,widget->width(),widget->height());
    widget->show();
    this->close();
}

/**
 * \brief    Opens game 2 
*/
void Games::opengame2() {
    Game2 *widget = new Game2(nullptr, QString::fromStdString(username));
    QDesktopWidget w1;
    QRect screenGeometry = w1.screenGeometry();
    int w = screenGeometry.width();
    int h = screenGeometry.height();
    widget->setGeometry((w-widget->width())/2,(h-widget->height())/2,widget->width(),widget->height());
    widget->show();
    this->close();
}
/**
 * \brief    Opens game 1 logs
*/
void Games::openhistorygame1() {
    GameHistory *widget = new GameHistory(username, 1);
    QDesktopWidget w1;
    QRect screenGeometry = w1.screenGeometry();
    int w = screenGeometry.width();
    int h = screenGeometry.height();
    widget->setGeometry((w-widget->width())/2,(h-widget->height())/2,widget->width(),widget->height());
    widget->show();
    this->close();
}

/**
 * \brief    Opens game 2 logs 
*/
void Games::openhistorygame2() {
    GameHistory *widget = new GameHistory(username, 2);
    QDesktopWidget w1;
    QRect screenGeometry = w1.screenGeometry();
    int w = screenGeometry.width();
    int h = screenGeometry.height();
    widget->setGeometry((w-widget->width())/2,(h-widget->height())/2,widget->width(),widget->height());
    widget->show();
    this->close();
}

/**
 * \brief    Opens Game 1 when F1 is pressed 
*/
void Games::keyPressEvent(QKeyEvent* event) {
    if (event->key() == Qt::Key_F1) {
        opengame1();
    }
}
