/**
 * \file InitialWidget.h
 * \brief The InitialWidget class is the first widget to open it is the page where the user can sign in, sign up or play as guest
 */
#ifndef INITALWIDGET_H
#define INITALWIDGET_H

#include <QWidget>
#include <QtWidgets>
#include <iostream>
#include <fstream>
#include <QString>
#include <QFile>
#include <QTextStream>
#include <QKeyEvent>
#include "SignupWidget.h"
#include "Games.h"
using namespace std;

class InitialWidget : public QWidget
{
    Q_OBJECT
public:
    explicit InitialWidget(QWidget *parent = nullptr);
    void keyPressEvent(QKeyEvent* event);
private:
    QLabel *username;
    QLabel *password;
    QLineEdit *usernamel;
    QLineEdit *passwordl;
    QGridLayout *grid;
    QPushButton *signupbutton;
    QPushButton *signinbutton;
    QPushButton *guestbutton;

public slots:
    void signup();
    void signin();
    void guest();
    string checkSignIn();

};

#endif // INITALWIDGET_H
