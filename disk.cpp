#include "disk.h"
#include <QKeyEvent>
/**
 * \file disk.cpp
 * \brief disk.cpp creates the Disk class to be used
 * \param parent a null pointer given to the QObject constructer, widget the widget that contains disk, parentscene the scene that contains disk, uname, the user name of the current user, value an int indicating what value disk this is
 */
Disk::Disk(QObject *parent, QWidget *widget, QGraphicsScene *scene, QString name, int val) : QObject(parent), widget(widget), parentscene(scene), uname(name), value(val) {

    if(value==0){
        setPixmap((QPixmap(":/images/redcircle.jpg")).scaled(81,82));
    } else if(value==1){
        setPixmap((QPixmap(":/images/greencircle.jpg")).scaled(81,82));
    } else if(value==2){
        setPixmap((QPixmap(":/images/bluecircle.jpg")).scaled(81,82));
    }
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(200);
    timer2 = new QTimer(this);
    connect(timer2, SIGNAL(timeout()), this, SLOT(changeSpeed()));
    timer2->start(100);
    reached30=0;
    reached60=0;
    reached90=0;
    reached120=0;
    changeamount=10;

}
/**
 * \brief checks if the disk is out of range and removes it
 *
 * everytime the timer runs out this function will be called
 * and it will check if the disk is out of the range of the scene
 * if true it will be removed and deleted
 */
void Disk::update(){
    Game2Scene *pscene = (Game2Scene *) parentscene;
    if (y() + 10 >= 900) {
        pscene->updatemissed();
        scene()->removeItem(this);
        pscene->thequeue.dequeue();
        delete this;
    } else {
        setPos(x(), y() + changeamount);
    }
}

void Disk::changeSpeed(){
    Game2Scene *pscene = (Game2Scene *) parentscene;
    if(pscene->score >= 30 && pscene->score < 60){
        if(reached30==0){
            timer->start(100);
            reached30=1;
            pscene->theme.setMedia(QUrl("qrc:/sounds/03. Game Theme (50 Left).mp3"));
        }
    } else if(pscene->score >= 60 && pscene->score < 90){
        if(reached60==0){
            timer->start(50);
            reached60=1;
        }
    } else if(pscene->score >= 90 && pscene->score < 120){
        if(reached90==0){
            timer->start(25);
            reached90=1;
            pscene->theme.setMedia(QUrl("qrc:/sounds/04. Game Theme (10 Left).mp3"));
        }
    } else if(pscene->score >= 120){
        if(reached120==0){
            timer->start(13);
            reached120=1;
        }
    }

}
