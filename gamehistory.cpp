#include "gamehistory.h"
/**
 * \file gamehistory.cpp
 * \brief gamehistory.cpp Display's games' logs
 * \param username a string the username of the current user, gamenum an int that indicates what game this is
 */
GameHistory::GameHistory(string username, int gamenum) {
    uname = QString::fromStdString(username);
    gamenumber = gamenum;
    highscore = 0;
    QVector<QString> history;
    int rows=0;
    leave = new QPushButton("leave");
    if(gamenumber==1){
        QFile file("/home/eece435l/omar_samer/databases/historygame1.txt");
        QString line;
        file.open(QIODevice::ReadOnly);
        if(!file.isOpen()){
            qDebug() << "not open";
        }
        QTextStream in(&file);
        while (!in.atEnd()) {
            line = in.readLine();
            QVector<int> loc = findLocations(line);
            if(line.mid(loc.at(2)+1,line.size()-loc.at(2)-1).toInt()>highscore){
                highscore=line.mid(loc.at(2)+1,line.size()-loc.at(2)-1).toInt();
            }
            if(line.toStdString().substr(0,username.size())==username) {
                history.push_back(line);;
                rows+=1;
            }
        }
        file.close();
        table = new QTableWidget(rows,3);
        colum_names << "date";
        colum_names << "outcome";
        colum_names << "score";
        table->setHorizontalHeaderLabels(colum_names);
        QTableWidgetItem *t[rows][3];
        for (int i=0;i<rows;i++) {
            QString tmp = history.at(i);
            QVector<int> loc = findLocations(tmp);
            t[i][0] = new QTableWidgetItem(tmp.mid(loc.at(0)+1,loc.at(1)-loc.at(0)-1));
            table->setItem(i,0,t[i][0]);
            t[i][1] = new QTableWidgetItem(tmp.mid(loc.at(1)+1,loc.at(2)-loc.at(1)-1));
            table->setItem(i,1,t[i][1]);
            t[i][2] = new QTableWidgetItem(tmp.mid(loc.at(2)+1,tmp.size()-loc.at(2)-1));
            table->setItem(i,2,t[i][2]);
    
        }
        name = new QLabel("        High Score: " + QString::number(highscore) + "                       " + QString::fromStdString(username) + "'s History");
        grid = new QGridLayout();
        grid->addWidget(name,0,0,1,3);
        grid->addWidget(table,1,0,1,3);
        grid->addWidget(leave,2,2);
        setLayout(grid);
        setFixedWidth(348);
        QObject::connect(leave, SIGNAL(clicked()), this, SLOT(leavef()));
    } else if(gamenumber==2){
        QFile file("/home/eece435l/omar_samer/databases/historygame2.txt");
        QString line;
        file.open(QIODevice::ReadOnly);
        if(!file.isOpen()){
            qDebug() << "not open";
        }
        QTextStream in(&file);
        while (!in.atEnd()) {
            line = in.readLine();
            QVector<int> loc = findLocations(line);
            if(line.mid(loc.at(2)+1,line.size()-loc.at(2)-1).toInt()>highscore){
                highscore=line.mid(loc.at(2)+1,line.size()-loc.at(2)-1).toInt();
            }
            if(line.toStdString().substr(0,username.size())==username) {
                history.push_back(line);;
                rows+=1;
            }
        }
        file.close();
        table = new QTableWidget(rows,3);
        colum_names << "date";
        colum_names << "outcome";
        colum_names << "score";
        table->setHorizontalHeaderLabels(colum_names);
        QTableWidgetItem *t[rows][3];
        for (int i=0;i<rows;i++) {
            QString tmp = history.at(i);
            QVector<int> loc = findLocations(tmp);
            t[i][0] = new QTableWidgetItem(tmp.mid(loc.at(0)+1,loc.at(1)-loc.at(0)-1));
            table->setItem(i,0,t[i][0]);
            t[i][1] = new QTableWidgetItem(tmp.mid(loc.at(1)+1,loc.at(2)-loc.at(1)-1));
            table->setItem(i,1,t[i][1]);
            t[i][2] = new QTableWidgetItem(tmp.mid(loc.at(2)+1,tmp.size()-loc.at(2)-1));
            table->setItem(i,2,t[i][2]);

        }
        name = new QLabel("        High Score: " + QString::number(highscore) + "                         " + QString::fromStdString(username) + "'s History");
        grid = new QGridLayout();
        grid->addWidget(name,0,0,1,3);
        grid->addWidget(table,1,0,1,3);
        grid->addWidget(leave,2,2);
        setLayout(grid);
        setFixedWidth(348);
        QObject::connect(leave, SIGNAL(clicked()), this, SLOT(leavef()));
    }
    this->setAttribute(Qt::WA_DeleteOnClose);
}

/**
 * \brief    Leave Game History
*/
void GameHistory::leavef() {
    Games *widget;
    QFile file("/home/eece435l/omar_samer/databases/creds_db.txt");
    QString line;
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream in(&file);
    while(!in.atEnd()) {
        line = in.readLine();
        if (line.mid(0,uname.size())==uname){
            widget = new Games(line.toStdString());
        }
    }
    QDesktopWidget w1;
    QRect screenGeometry = w1.screenGeometry();
    int w = screenGeometry.width();
    int h = screenGeometry.height();
    widget->setGeometry((w-widget->width())/2,(h-widget->height())/2,widget->width(),widget->height());
    widget->show();
    this->close();
}


