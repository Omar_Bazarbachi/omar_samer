#include <InitialWidget.h>
#include <SignupWidget.h>
#include <QApplication>
#include <iostream>
using namespace std;
/**
 * \mainpage Project_Omar_Samer
 * \author Omar Bazarbachi, Samer Hanna
 * \date 28-11-2021
 */
int main(int argc, char **argv){
    if (!(QDir("/home/eece435l/omar_samer/databases/").exists())) {
        QDir().mkdir("/home/eece435l/omar_samer/databases/"); }
    QFile file("/home/eece435l/omar_samer/databases/creds_db.txt");
    if (!file.exists()) { file.open(QIODevice::NewOnly); }
    QFile file1("/home/eece435l/omar_samer/databases/questions.txt");
    if (!file1.exists()) {
        file.open(QIODevice::NewOnly);
        QFile file("/home/eece435l/omar_samer/databases/questions.txt");
        file.open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text);
        file.resize(0);
        QTextStream out(&file);
        QString towrite = "Question1:Of the following, if statements, which one correctly executes three instructions if the condition is true?:If (x < 0) a = b * 2; y = x; z = a – y;:{ if (x < 0) a = b * 2; y = x; z = a – y;:If{ (x < 0) a = b * 2; y = x; z = a – y ;:If (x < 0) { a = b * 2; y = x; z = a – y; }:d"
                          "\nQuestion2:_______ is the process of finding errors and fixing them within a program.:Compiling:Executing:Debugging:Scanning:c"
                          "\nQuestion3:Kim has just constructed her first for loop within the Java language.  Which of the following is not a required part of a for loop?:Initialization:Condition:Variable:increment:c"
                          "\nQuestion4:Which command will stop an infinite loop?:Alt - C:Shift - C:Esc:Ctrl - C:d"
                          "\nQuestion5:A loop that never ends is referred to as a(n)_________.:While loop:Infinite loop:Recursive loop:for loop:b"
                          "\nQuestion6:q:q:q:q:q:b"
                          "\nQuestion7:q:q:q:q:q:c"
                          "\nQuestion8:q:q:q:q:q:d"
                          "\nQuestion9:q:q:q:q:q:a"
                          "\nQuestion10:q:q:q:q:q:b";
        out << towrite;
        file.close();
    }
    QFile file2("/home/eece435l/omar_samer/databases/historygame1.txt");
    if (!file2.exists()) { file.open(QIODevice::NewOnly); }
    QApplication app (argc, argv);
    InitialWidget *widget= new InitialWidget();
    QDesktopWidget w1;
    QRect screenGeometry = w1.screenGeometry();
    int w = screenGeometry.width();
    int h = screenGeometry.height();
    widget->setGeometry((w-widget->width())/2,(h-widget->height())/2,widget->width(),widget->height());
    widget->show();
    return app.exec();
}
