#include "game1scene.h"
/**
 * \file game1scene.cpp
 * \brief game1scene.cpp Sets the scene for Game1
 * \param parent a null pointer given to the QGraphicsScene constructer, widget a widget that is the parent of this scene, uname a QString that is the user name of the current user
 */
Game1Scene::Game1Scene(QGraphicsScene *parent, QWidget *widget, QString name) : QGraphicsScene(parent), widget(widget), uname(name) {

    setBackgroundBrush(QBrush(QImage(":/images/canvas.png").scaledToHeight(500).scaledToWidth(1000)));
    setSceneRect(0,0,1000,500);
    thetarget = new QGraphicsPixmapItem();
    thetarget->setPixmap((QPixmap(":/images/target.png")).scaled(81,82));
    thetarget->setFlag(QGraphicsPixmapItem::ItemIsFocusable);
    thetarget->setFocus();
    thetarget->setPos(560, 130);
    addItem(thetarget);
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(1500);
    srand(time(0));
    int left=6;
    if(largeshipside==0){
        used[largeshipposition]=1;
        used[largeshipposition+1]=1;
        used[largeshipposition+2]=1;
        used[largeshipposition+3]=1;
    }
    else{
        used[largeshipposition]=1;
        used[largeshipposition+4]=1;
        used[largeshipposition+8]=1;
        used[largeshipposition+12]=1;
    }
    while(left!=0){
        int tofill=rand()%16;
        if(used[tofill]==0){
            used[tofill]=1;
            left-=1;
        }
    }

    QFile file("/home/eece435l/omar_samer/databases/questions.txt");
    file.open(QIODevice::ReadWrite);
    QTextStream in(&file);
    int j=0;
    while(!in.atEnd()){
        line[j] = in.readLine();
        j++;
    }
}

void Game1Scene::keyPressEvent(QKeyEvent* event) {
    if (event->key() == Qt::Key_Right) {
        if (thetarget->x() < 800) {
            thetarget->setPos(thetarget->x() + 84, thetarget->y());
            cell+=1;
        }
    }
    if (event->key() == Qt::Key_Left) {
        if (thetarget->x() > 500) {
            thetarget->setPos(thetarget->x() - 84, thetarget->y());
            cell-=1;
        }
    }
    if (event->key() == Qt::Key_Up) {
        if (thetarget->y() > 130) {
            thetarget->setPos(thetarget->x(), thetarget->y()-85);
            cell-=4;
        }
    }
    if (event->key() == Qt::Key_Down) {
        if (thetarget->y() < 340) {
            thetarget->setPos(thetarget->x(), thetarget->y()+85);
            cell+=4;
        }
    }
    if (event->key() == Qt::Key_Return || event->key() == Qt::Key_Enter) {
        if(used[cell]==1){
            arehit++;
            AskingQuestion *ask = new AskingQuestion(this->widget,line[k]);
            QDesktopWidget w1;
            QRect screenGeometry = w1.screenGeometry();
            int w = screenGeometry.width();
            int h = screenGeometry.height();
            ask->setGeometry((w-ask->width())/2,(h-ask->height())/2,ask->width(),ask->height());
            ask->show();
            int *correct = &(ask->isCorrect);
            k++;
            if (ask->exec())
                ask->close();
            if(*correct==-1){
                hit = new QGraphicsPixmapItem();
                hit->setPixmap((QPixmap(":/images/green.png")).scaled(81,82));
                hit->setX(thetarget->x());
                hit->setY(thetarget->y());
                addItem(hit);
                bool newship = false;
                int pos[2];
                if(missed < 7){
                    while(!newship){
                        int i=rand()%7;
                        if(shipshit[i]==0){
                            pos[0]=ships[i][0];
                            pos[1]=ships[i][1];
                            shipshit[i]=1;
                            newship=true;
                            missed+=1;
                        }
                    }
                    counter = new QGraphicsPixmapItem();
                    counter->setPixmap((QPixmap(":/images/red.jpeg")).scaled(81,82));
                    counter->setX(pos[0]);
                    counter->setY(pos[1]);
                    addItem(counter);
                }
            } else{
                hit = new QGraphicsPixmapItem();
                hit->setPixmap((QPixmap(":/images/red.png")).scaled(81,82));
                hit->setX(thetarget->x());
                hit->setY(thetarget->y());
                addItem(hit);
                correctanswers++;
            }
            if(arehit==10){
                if(correctanswers< 7){
                    Lost *widget = new Lost(this->widget,uname,correctanswers,1);
                    QDesktopWidget w1;
                    QRect screenGeometry = w1.screenGeometry();
                    int w = screenGeometry.width();
                    int h = screenGeometry.height();
                    widget->setGeometry((w-widget->width())/2,(h-widget->height())/2,widget->width(),widget->height());
                    widget->show();
                }
                else{
                    Won *widget = new Won(this->widget,uname,correctanswers,1);
                    QDesktopWidget w1;
                    QRect screenGeometry = w1.screenGeometry();
                    int w = screenGeometry.width();
                    int h = screenGeometry.height();
                    widget->setGeometry((w-widget->width())/2,(h-widget->height())/2,widget->width(),widget->height());
                    widget->show();
                }
            }
        }
        else{
            hit = new QGraphicsPixmapItem();
            hit->setPixmap((QPixmap(":/images/blue.png")).scaled(81,82));
            hit->setX(thetarget->x());
            hit->setY(thetarget->y());
            addItem(hit);
        }
    }
}
