#include "questiondialogue.h"
/**
 * \file questiondialogue.cpp
 * \brief questiondialogue.cpp creates the MainScene class to be used
 * \param parent a null pointer given to the QWidget constructer
 */

QuestionDialogue::QuestionDialogue(QWidget *parent) : QWidget(parent) {

    prompt = new QLabel("Please input the questions");
    QFont font = prompt->font();
    font.setPointSize(20);
    font.setBold(true);
    prompt->setFont(font);

    QWidget *empty[10];

    submit = new QPushButton("submit");
    layout = new QGridLayout();
    layout->addWidget(prompt,0,4,1,3);

    for (int i=0;i<5;i++) {
        empty[i] = new QWidget;
        abuttons[i] = new QRadioButton();
        a[i] = new QLabel("a)");
        bbuttons[i] = new QRadioButton();
        b[i] = new QLabel("b)");
        cbuttons[i] = new QRadioButton();
        c[i] = new QLabel("c)");
        dbuttons[i] = new QRadioButton();
        d[i] = new QLabel("d)");
        buttongroup[i] = new QButtonGroup();
        buttongroup[i]->addButton(abuttons[i]);
        buttongroup[i]->addButton(bbuttons[i]);
        buttongroup[i]->addButton(cbuttons[i]);
        buttongroup[i]->addButton(dbuttons[i]);
        QString str = QString::number(i+1);
        questionlabel[i] = new QLabel("Question" + str + ":");
        question[i] = new QLineEdit();
        answera[i] = new QLineEdit();
        answerb[i] = new QLineEdit();
        answerc[i] = new QLineEdit();
        answerd[i] = new QLineEdit();
        Ha[i]= new QHBoxLayout;
        Hb[i]= new QHBoxLayout;
        Hc[i]= new QHBoxLayout;
        Hd[i]= new QHBoxLayout;
        Ha[i]->addWidget(a[i]);
        Ha[i]->addWidget(abuttons[i]);
        Hb[i]->addWidget(b[i]);
        Hb[i]->addWidget(bbuttons[i]);
        Hc[i]->addWidget(c[i]);
        Hc[i]->addWidget(cbuttons[i]);
        Hd[i]->addWidget(d[i]);
        Hd[i]->addWidget(dbuttons[i]);
        layout->addWidget(questionlabel[i],1,i*2);
        layout->addWidget(question[i],1,i*2+1);
        layout->addLayout(Ha[i],2,i*2);
        layout->addWidget(answera[i],2,i*2+1);
        layout->addLayout(Hb[i],3,i*2);
        layout->addWidget(answerb[i],3,i*2+1);
        layout->addLayout(Hc[i],4,i*2);
        layout->addWidget(answerc[i],4,i*2+1);
        layout->addLayout(Hd[i],5,i*2);
        layout->addWidget(answerd[i],5,i*2+1);
        layout->addWidget(empty[i],6,i*2);

    }

    for (int i=5;i<10;i++) {
        empty[i] = new QWidget;
        abuttons[i] = new QRadioButton();
        a[i] = new QLabel("a)");
        bbuttons[i] = new QRadioButton();
        b[i] = new QLabel("b)");
        cbuttons[i] = new QRadioButton();
        c[i] = new QLabel("c)");
        dbuttons[i] = new QRadioButton();
        d[i] = new QLabel("d)");
        buttongroup[i] = new QButtonGroup();
        buttongroup[i]->addButton(abuttons[i]);
        buttongroup[i]->addButton(bbuttons[i]);
        buttongroup[i]->addButton(cbuttons[i]);
        buttongroup[i]->addButton(dbuttons[i]);
        QString str = QString::number(i+1);
        questionlabel[i] = new QLabel("Question" + str + ":");
        question[i] = new QLineEdit();
        answera[i] = new QLineEdit();
        answerb[i] = new QLineEdit();
        answerc[i] = new QLineEdit();
        answerd[i] = new QLineEdit();
        Ha[i]= new QHBoxLayout;
        Hb[i]= new QHBoxLayout;
        Hc[i]= new QHBoxLayout;
        Hd[i]= new QHBoxLayout;
        Ha[i]->addWidget(a[i]);
        Ha[i]->addWidget(abuttons[i]);
        Hb[i]->addWidget(b[i]);
        Hb[i]->addWidget(bbuttons[i]);
        Hc[i]->addWidget(c[i]);
        Hc[i]->addWidget(cbuttons[i]);
        Hd[i]->addWidget(d[i]);
        Hd[i]->addWidget(dbuttons[i]);
        int j=i-5;
        layout->addWidget(questionlabel[i],7,j*2);
        layout->addWidget(question[i],7,j*2+1);
        layout->addLayout(Ha[i],8,j*2);
        layout->addWidget(answera[i],8,j*2+1);
        layout->addLayout(Hb[i],9,j*2);
        layout->addWidget(answerb[i],9,j*2+1);
        layout->addLayout(Hc[i],10,j*2);
        layout->addWidget(answerc[i],10,j*2+1);
        layout->addLayout(Hd[i],11,j*2);
        layout->addWidget(answerd[i],11,j*2+1);
        layout->addWidget(empty[i],12,j*2);

    }

    layout->addWidget(submit,12,9);

    setLayout(layout);
    setFixedSize(1920,400);
    QObject::connect(submit, SIGNAL(clicked()), this, SLOT(opengame1()));
    this->setAttribute(Qt::WA_DeleteOnClose);

}

void QuestionDialogue::opengame1() {
    SaveQuestions();
    Game1 *widget = new Game1();
    QDesktopWidget w1;
    QRect screenGeometry = w1.screenGeometry();
    int w = screenGeometry.width();
    int h = screenGeometry.height();
    widget->setGeometry((w-widget->width())/2,(h-widget->height())/2,widget->width(),widget->height());
    widget->show();
    this->close();
    this->~QuestionDialogue();
}

void QuestionDialogue::SaveQuestions(){
    QFile file("/home/eece435l/omar_samer/databases/questions.txt");
    file.open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text);
    file.resize(0);
    QTextStream out(&file);
    QString towrite[10];
    for (int i=0;i<10;i++) {
        if(abuttons[i]->isChecked()){
             towrite[i] = "Question"+ QString::number(i+1) +":" + question[i]->text() +
                          ":" + answera[i]->text() + ":" + answerb[i]->text() +
                          ":" + answerc[i]->text() + ":" + answerd[i]->text() + ":a" ;
        } else if(bbuttons[i]->isChecked()){
             towrite[i] = "Question"+ QString::number(i+1) +":" + question[i]->text() +
                          ":" + answera[i]->text() + ":" + answerb[i]->text() +
                          ":" + answerc[i]->text() + ":" + answerd[i]->text() + ":b" ;
        } else if(cbuttons[i]->isChecked()){
             towrite[i] = "Question"+ QString::number(i+1) +":" + question[i]->text() +
                          ":" + answera[i]->text() + ":" + answerb[i]->text() +
                          ":" + answerc[i]->text() + ":" + answerd[i]->text() + ":c" ;
        } else if(dbuttons[i]->isChecked()){
            towrite[i] = "Question"+ QString::number(i+1) +":" + question[i]->text() +
                         ":" + answera[i]->text() + ":" + answerb[i]->text() +
                         ":" + answerc[i]->text() + ":" + answerd[i]->text() + ":d" ;
        } else{
            towrite[i] = "Question"+ QString::number(i+1) +":" + question[i]->text() +
                         ":" + answera[i]->text() + ":" + answerb[i]->text() +
                         ":" + answerc[i]->text() + ":" + answerd[i]->text() + ":" ;
        }
    }
    for (int i=0;i<10;i++) {
        out << towrite[i] << endl;
    }
    file.close();
}

