#include "SignupWidget.h"
#include <iostream>
#include <fstream>
using namespace std;
/**
 * \file SignupWidget.cpp
 * \brief SignupWidget.cpp Initializes signup GUI
 * \param parent a null pointer given to the QWidget constructer
 */
SignupWidget::SignupWidget(QWidget *parent) : QWidget(parent) {
    first = new QLabel("First Name:");
    last = new QLabel("Last Name:");
    username = new QLabel("Username:");
    password = new QLabel("Password:");
    password2 = new QLabel("Confirm Password:");
    dateofbirth = new QLabel("Date Of Birth:");
    phone = new QLabel("Phone Number:");
    profilepic = new QLabel("Upload A Profile Picture:");
    firstl = new QLineEdit();
    lastl = new QLineEdit();
    usernamel = new QLineEdit();
    passwordl = new QLineEdit();
    passwordl->setEchoMode(QLineEdit::Password);
    password2l = new QLineEdit();
    password2l->setEchoMode(QLineEdit::Password);
    countrycode = new QLineEdit();
    countrycode->setFixedWidth(40);
    phonenumber = new QLineEdit();
    date = new QDateEdit();
    gender = new QLabel("Gender");
    male = new QRadioButton("Male");
    female = new QRadioButton("Female");
    submit = new QPushButton("Submit");
    uploadpic = new QPushButton("upload Profile Picture");
    grid = new QGridLayout;
    radio = new QGroupBox;
    radiover = new QVBoxLayout;
    number = new QHBoxLayout;
    radiover->addWidget(male);
    radiover->addWidget(female);
    radio->setLayout(radiover);
    number->addWidget(countrycode);
    number->addWidget(phonenumber);
    grid->addWidget(first,0,0);
    grid->addWidget(firstl,0,1);
    grid->addWidget(last,1,0);
    grid->addWidget(lastl,1,1);
    grid->addWidget(username,2,0);
    grid->addWidget(usernamel,2,1);
    grid->addWidget(password,3,0);
    grid->addWidget(passwordl,3,1);
    grid->addWidget(password2,4,0);
    grid->addWidget(password2l,4,1);
    grid->addWidget(dateofbirth,5,0);
    grid->addWidget(date,5,1);
    grid->addWidget(gender,6,0);
    grid->addWidget(radio,6,1);
    grid->addWidget(phone,8,0);
    grid->addLayout(number,8,1);
    grid->addWidget(profilepic,9,0);
    grid->addWidget(uploadpic,9,1);
    grid->addWidget(submit,10,2);
    empty=new QLabel();
    grid->addWidget(empty,11,0,1,2);
    setLayout(grid);
    QObject::connect(submit, SIGNAL(clicked()), this, SLOT(signup()));
    QObject::connect(uploadpic, SIGNAL(clicked()), this, SLOT(uploadpicture()));
    setFixedSize(500,400);
    this->setAttribute(Qt::WA_DeleteOnClose);
}

/**
 * \brief    Handles picture uploading
*/
void SignupWidget::uploadpicture(){
    QFileDialog *pic = new QFileDialog;
    path=pic->getOpenFileName().toStdString();
}
void SignupWidget::signup() {
    string cred=add2DB();
    if (cred=="") return;
    Games *widget = new Games(cred);
    QDesktopWidget w1;
    QRect screenGeometry = w1.screenGeometry();
    int w = screenGeometry.width();
    int h = screenGeometry.height();
    widget->setGeometry((w-widget->width())/2,(h-widget->height())/2,widget->width(),widget->height());
    widget->show();
    this->close();
}

/**
 * \brief    Handles adding users to database
*/
string SignupWidget::add2DB() {
    if(passwordl->text()!=password2l->text()) {
        empty->setText("Passwords not matching");
        return "";
    }
    if(firstl->text().size()==0 || last->text().size()==0) {
        empty->setText("Please fill all fields");
        return "";
    }
    if(usernamel->text().size()==0) {
        empty->setText("Please fill all fields");
        return "";
    }
    if(passwordl->text().size()<8) {
        empty->setText("Password Too Short");
        return "";
    }
    string str = passwordl->text().toStdString();
    if(!((any_of(str.begin(), str.end(), [](char i){return (int)i<91 && (int)i>64;})) &&
         (any_of(str.begin(), str.end(), [](char i){return (int)i<123 && (int)i>96;})) &&
         (any_of(str.begin(), str.end(), [](char i){return (int)i<58 && (int)i>47;})))) {
        empty->setText("Password must contain Uppercase, Lowercase, and number");
        return "";
    }
    string gender="";
    if(male->isChecked()){
        gender="Male";
    }
    else{
        gender="Female";
    }
    string creds = usernamel->text().toStdString() + ":"
                 + passwordl->text().toStdString() + ":"
                 + firstl->text().toStdString() + ":"
                 + lastl->text().toStdString() + ":"
                 + countrycode->text().toStdString() + ":"
                 + phonenumber->text().toStdString() + ":"
                 + date->text().toStdString() + ":"
                 + path + ":"
                 + gender;
    QFile file("/home/eece435l/omar_samer/databases/creds_db.txt");
    QString line;  
    file.open(QIODevice::ReadOnly);
    if(!file.isOpen()){
        qDebug() << "not open";
    }
    QTextStream in(&file);
    while (!in.atEnd()) {
        line = in.readLine();
        if(line.toStdString().substr(0,usernamel->text().toStdString().size())==usernamel->text().toStdString()) {
            empty->setText("this username already exists");
            return "";
        }
    }
    file.close();
    file.open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text);
    if(!file.isOpen()){
        qDebug() << "not open";
    }
    QTextStream out(&file);
    out << QString::fromStdString(creds) << endl;
    file.close();
    return creds;
}

/**
 * \brief    Calls signup() when the corresponding key is pressed
*/
void SignupWidget::keyPressEvent(QKeyEvent* event) {
    if (event->key() == Qt::Key_Return || event->key() == Qt::Key_Enter) {
        signup();
    }
}

