#include "won.h"
/**
 * \file won.cpp
 * \brief won.cpp Initialzes the window that pops up when the user has won
 * \param widget a QWidget the parent widget, uname a QString the username of the current user, score an int the score of the game, game an int the number of the game that was played
 */
Won::Won( QWidget *widget, QString name, int score, int game) : wid(widget), uname(name), score(score), game(game){

    message = new QLabel("You Win!");
    leave = new QPushButton("leave");
    grid = new QGridLayout;
    grid->addWidget(message);
    grid->addWidget(leave);
    setLayout(grid);
    setFixedSize(100,100);
    QObject::connect(leave, SIGNAL(clicked()), this, SLOT(leavef()));
    this->setAttribute(Qt::WA_DeleteOnClose);
}

/**
 * \brief    Leaves Game
*/
void Won::leavef() {
    if(uname != ""){
        saveScore();
        Games *widget;
        QFile file("/home/eece435l/omar_samer/databases/creds_db.txt");
        QString line;
        file.open(QIODevice::ReadOnly | QIODevice::Text);
        QTextStream in(&file);
        while(!in.atEnd()) {
            line = in.readLine();
            if (line.mid(0,uname.size())==uname){
                widget = new Games(line.toStdString());
            }
        }
        QDesktopWidget w1;
        QRect screenGeometry = w1.screenGeometry();
        int w = screenGeometry.width();
        int h = screenGeometry.height();
        widget->setGeometry((w-widget->width())/2,(h-widget->height())/2,widget->width(),widget->height());
        widget->show();
        if(game==1){
            Game1 *todelete = (Game1 *) wid;
            delete todelete->scene;
        } else {
            Game2 *todelete = (Game2 *) wid;
            delete todelete->scene;
        }
        wid->close();
        this->close();
    } else{
        Games *widget = new Games();
        QDesktopWidget w1;
        QRect screenGeometry = w1.screenGeometry();
        int w = screenGeometry.width();
        int h = screenGeometry.height();
        widget->setGeometry((w-widget->width())/2,(h-widget->height())/2,widget->width(),widget->height());
        widget->show();
        if(game==1){
            Game1 *todelete = (Game1 *) wid;
            delete todelete->scene;
        } else {
            Game2 *todelete = (Game2 *) wid;
            delete todelete->scene;
        }
        wid->close();
        this->close();
    }
}

/**
 * \brief    Logs the user's score
*/
void Won::saveScore(){
    if(game==1){
        QFile file("/home/eece435l/omar_samer/databases/historygame1.txt");
        file.open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text);
        if(!file.isOpen()){
            qDebug() << "not open";
        }
        QTextStream out(&file);
        out << uname + ":" + QDate::currentDate().toString() + ":won:" + QString::number(score) << endl;
        file.close();
    } else{
        QFile file("/home/eece435l/omar_samer/databases/historygame2.txt");
        file.open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text);
        if(!file.isOpen()){
            qDebug() << "not open";
        }
        QTextStream out(&file);
        out << uname + ":" + QDate::currentDate().toString() + ":won:" + QString::number(score) << endl;
        file.close();
    }
}

