/**
 * \file game2scene.h
 * \brief The Game2Scene class acts as the scene of Game 2
 */
#ifndef GAME2SCENE_H
#define GAME2SCENE_H

#include <QGraphicsScene>
#include <QTimer>
#include <QMediaPlayer>
#include "Games.h"
#include "game2.h"
#include "disk.h"
#include <cstdlib>
#include <time.h>
#include "lost.h"
#include "won.h"

class Game2Scene : public QGraphicsScene
{
    Q_OBJECT
public:
    explicit Game2Scene(QGraphicsScene *parent= 0, QWidget *widget=nullptr, QString name="") ;
    void updatescore(int i);
    void updatemissed();
    void keyPressEvent(QKeyEvent *event);
    int score;
    QTimer* timer;
    QQueue<QObject*> thequeue;
    QMediaPlayer theme;

private:
    QWidget* widget;
    QString uname;
    QTimer* timer2;
    QLabel *scoretoshow;
    int missed;
signals:

public slots:
    void update();
    void checklost();

};

#endif // GAME2SCENE_H
