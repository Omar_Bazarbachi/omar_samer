/**
 * \file game1scene.h
 * \brief The Game1Scene class acts as the scene of Game 1
 */
#ifndef GAME1SCENE_H
#define GAME1SCENE_H

#include <QGraphicsScene>
#include <QTimer>
#include "Games.h"
#include "game1.h"
#include "askingquestion.h"

class Game1Scene : public QGraphicsScene
{
    Q_OBJECT
public:
    explicit Game1Scene(QGraphicsScene *parent = 0, QWidget *widget=nullptr, QString name="") ;
private:
    void keyPressEvent(QKeyEvent* event);
    QGraphicsPixmapItem* thetarget;
    QWidget* widget;
    QGraphicsPixmapItem *hit;
    QString uname;
    QGraphicsPixmapItem *counter;
    QTimer* timer;
    int cell=0;
    int used[16] = {0};
    int ships[7][2]={{107,215},{275,215},{359,215},{107,385},{191,385},{275,385},{359,385}};
    int shipshit[7]={0};
    int arehit=0;
    int correctanswers=0;
    int largeshipposition = rand()%4;
    int largeshipside = rand()%2;
    int k=0;
    int missed=0;
    QString line[10];
signals:

public slots:
};

#endif // GAME1SCENE_H
