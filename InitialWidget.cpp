#include "InitialWidget.h"
using namespace std;
/**
 * \file InitialWidget.cpp
 * \brief InitialWidget.cpp Initializes The first widget's GUI
 * \param parent a null pointer given to the QWidget constructer
 */
InitialWidget::InitialWidget(QWidget *parent) : QWidget(parent) {
    username = new QLabel("Username:");
    password = new QLabel("Password:");
    usernamel = new QLineEdit();
    passwordl = new QLineEdit();
    passwordl->setEchoMode(QLineEdit::Password);
    grid = new QGridLayout;
    signupbutton = new QPushButton("Sign Up");
    signinbutton = new QPushButton("Sign In");
    guestbutton = new QPushButton("Play as Guest");
    grid->addWidget(username,0,0);
    grid->addWidget(usernamel,0,1);
    grid->addWidget(password,1,0);
    grid->addWidget(passwordl,1,1);
    grid->addWidget(signinbutton,2,2);
    grid->addWidget(signupbutton,3,1);
    grid->addWidget(guestbutton,4,1);
    setLayout(grid);
    QObject::connect(signupbutton, SIGNAL(clicked()), this, SLOT(signup()));
    QObject::connect(signinbutton, SIGNAL(clicked()), this, SLOT(signin()));
    QObject::connect(guestbutton, SIGNAL(clicked()), this, SLOT(guest()));
    setFixedSize(350,180);
    this->setAttribute(Qt::WA_DeleteOnClose);
}

/**
 * \brief    Handles Sign Up 
*/
void InitialWidget::signup() {
    SignupWidget *widget = new SignupWidget();
    QDesktopWidget w1;
    QRect screenGeometry = w1.screenGeometry();
    int w = screenGeometry.width();
    int h = screenGeometry.height();
    widget->setGeometry((w-widget->width())/2,(h-widget->height())/2,widget->width(),widget->height());
    widget->show();
    this->close();
}

/**
 * \brief    Handles Sign In 
*/
void InitialWidget::signin() {
    string cred = checkSignIn();
    if(cred=="") return;
    Games *widget = new Games(cred);
    QDesktopWidget w1;
    QRect screenGeometry = w1.screenGeometry();
    int w = screenGeometry.width();
    int h = screenGeometry.height();
    widget->setGeometry((w-widget->width())/2,(h-widget->height())/2,widget->width(),widget->height());
    widget->show();
    this->close();
}

/**
 * \brief    Handles Guest 
*/
void InitialWidget::guest() {
    Games *widget = new Games();
    QDesktopWidget w1;
    QRect screenGeometry = w1.screenGeometry();
    int w = screenGeometry.width();
    int h = screenGeometry.height();
    widget->setGeometry((w-widget->width())/2,(h-widget->height())/2,widget->width(),widget->height());
    widget->show();
    this->close();
}

/**
 * \brief    Verifies that user trying to sign In already signed Up 
*/
string InitialWidget::checkSignIn() {
    QFile file("/home/eece435l/omar_samer/databases/creds_db.txt");
    QString line;
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream in(&file);
    string creds = usernamel->text().toStdString() + ":" + passwordl->text().toStdString();
    while(!in.atEnd()) {
        line = in.readLine();
        if (line.toStdString().substr(0,creds.size())==creds){
            return line.toStdString();
        }
    }
    return "";
}

/**
 * \brief    Calls signin() when the corresponding key is pressed 
*/
void InitialWidget::keyPressEvent(QKeyEvent* event) {
    if (event->key() == Qt::Key_Return || event->key() == Qt::Key_Enter) {
        signin();
    }
}

